#!/bin/bash

wget --ca-certificate=initialise/lazyslug "https://lazyslug.com/lifeview/plugin/js/release/lv-plugin.js"
cp lv-plugin.js "src/main/webapp/js/lv-plugin.js"

git clone "https://gitlab.com/apgoucher/apgmera.git"

cd apgmera
./recompile.sh --mingw
cd -

cp "apgmera/lifelib/genera/genuslist.py" "src/main/webapp/js/genuslist.py"

FILENAME="$( echo apgmera/apg* )"

if [ -f "$FILENAME" ]; then
cp "$FILENAME" "src/main/webapp/binaries/apgluxe-windows-x86_64.exe"
APGLUXE_VERSION="$( grep -o 'v[4-9][^-]*' apgmera/main.cpp )"
sed -i "s/v4.xxxx/$APGLUXE_VERSION/g" "src/main/java/com/cp4space/catagolue/servlets/ApgsearchServlet.java"
else
printf "\033[31;m Error: could not find precompiled apgsearch executable \033[0m\n"
fi

exit 0
