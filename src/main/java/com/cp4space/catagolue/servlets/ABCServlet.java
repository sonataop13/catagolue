
package com.cp4space.catagolue.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.lang.Iterable;
import java.util.List;
import java.util.ArrayList;
import java.util.TreeSet;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.utils.SvgUtils;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;

public class ABCServlet extends HttpServlet {

    public void writeContent(PrintWriter writer, HttpServletRequest req) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        Key statsKey = KeyFactory.createKey("Difficulties", "censuslist");

        try {
            Entity statsEntity = datastore.get(statsKey);
            String content = ((Text) statsEntity.getProperty("data")).getValue();
            Date lastModified = (Date) statsEntity.getProperty("lastModified");

            Date currentTime = new Date();

            long elapsedMillis = currentTime.getTime() - lastModified.getTime();
            long elapsedSeconds = elapsedMillis / 1000;

            if (elapsedSeconds < 21600) {
                String creationTime = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(lastModified));
                writer.println(content);
                return;
            }
        } catch (EntityNotFoundException e) {
            
        }

        ByteArrayOutputStream bbuffer = new ByteArrayOutputStream();
        PrintWriter writer2 = new PrintWriter(bbuffer);
        generateContent(writer2, req);
        writer2.close();

        String content = bbuffer.toString();

        Entity statsEntity = new Entity("Difficulties", "censuslist");
        Date date = new Date();
        statsEntity.setProperty("lastModified", date);
        statsEntity.setProperty("data", new Text(content));

        datastore.put(statsEntity);

        writer.println(content);
    }

    public static ArrayList<String> standardCensuses() {

        int totlength = 0;

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Query query = new Query("Census").addSort("lastModified", Query.SortDirection.DESCENDING).setKeysOnly();
        PreparedQuery preparedQuery = datastore.prepare(query);
        Iterable<Entity> censuses = preparedQuery.asIterable(FetchOptions.Builder.withLimit(100000));

        ArrayList<String> censusNames = new ArrayList<String>();

        Pattern validrule = Pattern.compile("[a-z][a-zA-Z0-9_-]+");
        Pattern validsym = Pattern.compile("([A-Z][a-zA-Z0-9_]*~)?[a-zA-Z0-9_+]+");
        for (Entity census : censuses) {
            String censusname = census.getKey().getName();
            String rulestring = censusname.split("/")[0];
            String symmetry = censusname.split("/")[1];
            String[] symParts = symmetry.split("-", 2);

            if (symParts.length >= 2) { continue; }

            boolean matchrule = validrule.matcher(rulestring).matches();
            boolean matchsym = validsym.matcher(symParts[0]).matches();

            if (matchrule && matchsym) {
                censusNames.add(censusname);
                totlength += (censusname.length() + 2);
                if (totlength > 980000) { break; }
            } else {
                datastore.delete(census.getKey());
            }
        }

        return censusNames;
    }

    public void generateContent(PrintWriter writer, HttpServletRequest req) {

        ArrayList<String> censusNames = standardCensuses();

        for (String censusname : censusNames) {
            writer.println(censusname);
        }
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/plain");
        PrintWriter writer = resp.getWriter();
        writeContent(writer, req);

    }
}
