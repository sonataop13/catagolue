
function decodeCanon(apgcode) {

    var i;
    var chars = "0123456789abcdefghijklmnopqrstuvwxyz"

    for (i = 0; i < apgcode.length; i++) {
        if (apgcode[i] == "_") {
            i += 1;
            break;
        }
    }
    
    var blank = 0;
    var x = 0;
    var y = 0;
    var cells = []
    
    for (/**/; i < apgcode.length; i++) {
   
        var c = apgcode[i];
        if (blank) {
            x += chars.indexOf(c);
            blank = 0;
        } else if (c == 'y') {
            x += 4;
            blank = 1;
        } else if (c == 'x') {
            x += 3;
        } else if (c == 'w') {
            x += 2;
        } else if (c == 'z') {
            x = 0;
            y += 5;
        } else {
            v = chars.indexOf(c);
            for (var j = 0; j < 5; j++)
                if (v & (1 << j)) {
                    cells.push(x);
                    cells.push(y+j);
                }
            x += 1;
        }
    }

    return cells;
}

function add_symbol(out_pair, num, symbol) {
    if (num == 0) return;
    if (num > 1) symbol = num.toString() + symbol;
    out_pair[1] += symbol.length;
    if (out_pair[1] > 77) {
        out_pair[0] += "<br>";
        out_pair[1] = symbol.length;
    }
    out_pair[0] += symbol;
}

function cells_to_rle(cells) {

    if (cells.length == 0) return "x = 0, y = 0, rule = B3/S23<br>!";

    var minx = 99999;
    var maxx = -99999;
    var miny = 99999;
    var maxy = -99999;

    for (var i = 0; i < cells.length; i += 2) {

        x = cells[i];
        y = cells[i+1];

        if (x > maxx) maxx = x;
        if (x < minx) minx = x;
        if (y > maxy) maxy = y;
        if (y < miny) miny = y;

    }

    var pairs = [];
    for (var i = 0; i < cells.length; i += 2)
        pairs.push((cells[i]-minx) + 1024 * cells[i+1]);
    pairs.sort(function(a,b){return a-b});

    for (var i = 0; i < pairs.length; i++) {
        cells[2*i] = pairs[i] & 1023;
        cells[2*i+1] = pairs[i] >> 10;
    }

    var w = maxx - minx + 1;
    var h = maxy - miny + 1;
    var output = "x = " + w.toString() + ", y = " + h.toString() + ", rule = B3/S23<br>";

    var x = cells[0];
    var y = cells[1];
    var on_start = cells[0];
    var off_start = 0;
    var out_pair = [output, 0];

    for (var i = 2; i < cells.length; i += 2) {

        if (cells[i] == x + 1 && cells[i+1] == y) {
            x++;
        } else {
            
            add_symbol(out_pair, on_start - off_start, "b");
            add_symbol(out_pair, x + 1 - on_start, "o");

            off_start = x + 1;

            var new_lines = cells[i+1] - y;
            if (new_lines > 0) {
                add_symbol(out_pair, new_lines, "$");
                off_start = 0;
            }

            x = on_start = cells[i];
            y = cells[i+1];
        }        
    }

    add_symbol(out_pair, on_start - off_start, "b");
    add_symbol(out_pair, x + 1 - on_start, "o");

    return out_pair[0] + "!";
    
}
